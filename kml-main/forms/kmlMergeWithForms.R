df <- df %>%
  dplyr::mutate(
    u_monthsDiagUppf = as.numeric(u_datbedom - d0_date) / 365.24 * 12,
    u_daysDiagDeath = as.numeric(vitalstatusdatum_estimat - d0_date),
    u_daysDiagDeath = dplyr::if_else(
      !(vitalstatus %in% 1),
      as.numeric(NA),
      u_daysDiagDeath
    )
  )

dfU <- df %>%
  dplyr::filter(
    u_inca %in% 0 | 
      !is.na(u_datbedom) | 
      !is.na(u_inrappdtm) | 
      !is.na(u_uppfoljningnr)
  ) %>% 
  dplyr::select(
    "pat_id",
    starts_with("u_")
  )

df <- df %>%
  dplyr::filter(!is.na(d0_date)) %>% 
  dplyr::arrange(pat_id, d0_date, desc(d0_inca)) %>% 
  dplyr::distinct(pat_id, .keep_all = TRUE) %>% 
  dplyr::select(-starts_with("u_"))

# 6 mån, om pre-INCA finns med
if (any(dfU$u_uppfoljningnr %in% 0.5)) {
  df <- df %>% 
    dplyr::left_join(
      kmlExtractForm(
        data = dfU, 
        uppfNum = 0.5, 
        monthsStart = -999, 
        monthsStop = -999, 
        prefix = "u6m_"
      ),
      by = "pat_id"
    ) %>% 
    dplyr::mutate(
      u6m_uppfoljningrapporterad = dplyr::if_else(
        is.na(u6m_uppfoljningrapporterad),
        0,
        u6m_uppfoljningrapporterad
      )
    )
}

df <- df %>% 
  
  # 1 år
  dplyr::left_join(
    kmlExtractForm(
      data = dfU, 
      uppfNum = c(1, 11), 
      monthsStart = 9, 
      monthsStop = 18, 
      prefix = "u1y_",
      special1y = TRUE
    ),
    by = "pat_id"
  ) %>% 
  dplyr::mutate(
    u1y_uppfoljningrapporterad = dplyr::if_else(
      is.na(u1y_uppfoljningrapporterad),
      0,
      u1y_uppfoljningrapporterad
    )
  ) %>% 
  
  # 2 år
  dplyr::left_join(
    kmlExtractForm(
      data = dfU, 
      uppfNum = 2, 
      monthsStart = 18, 
      monthsStop = 30, 
      prefix = "u2y_"
    ),
    by = "pat_id"
  ) %>% 
  dplyr::mutate(
    u2y_uppfoljningrapporterad = dplyr::if_else(
      is.na(u2y_uppfoljningrapporterad),
      0,
      u2y_uppfoljningrapporterad
    )
  ) %>% 
  
  # 5 år
  dplyr::left_join(
    kmlExtractForm(
      data = dfU, 
      uppfNum = 5, 
      monthsStart = 54, 
      monthsStop = 66, 
      prefix = "u5y_"
    ),
    by = "pat_id"
  ) %>% 
  dplyr::mutate(
    u5y_uppfoljningrapporterad = dplyr::if_else(
      is.na(u5y_uppfoljningrapporterad),
      0,
      u5y_uppfoljningrapporterad
    )
  ) %>% 
  
  # 10 år
  dplyr::left_join(
    kmlExtractForm(
      data = dfU, 
      uppfNum = 10, 
      monthsStart = 114, 
      monthsStop = 126, 
      prefix = "u10y_"
    ),
    by = "pat_id"
  ) %>% 
  dplyr::mutate(
    u10y_uppfoljningrapporterad = dplyr::if_else(
      is.na(u10y_uppfoljningrapporterad),
      0,
      u10y_uppfoljningrapporterad
    )
  )