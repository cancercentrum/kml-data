kmlExtractForm <- 
  function(
    data = NULL, 
    uppfNum = NULL, 
    monthsStart = NULL, 
    monthsStop = NULL, 
    prefix = "u_",
    special1y = FALSE
  ) {
    data$u_giltigVariabel <- 1 * (data$u_uppfoljningnr %in% uppfNum)
    data$u_giltigIntervall <- 1 * (
      !is.na(data$u_monthsDiagUppf) & 
        data$u_monthsDiagUppf >= monthsStart & 
        data$u_monthsDiagUppf < monthsStop
    )
    if (special1y) {
      # En uppföljning när som helst innan död inom ett år räknas
      data$u_giltigIntervall[!is.na(data$u_daysDiagDeath) & data$u_daysDiagDeath < 365.24] <- 1
    }
    data <- 
      subset(
        data,
        u_giltigVariabel %in% 1 | u_giltigIntervall %in% 1
      )
    data <- data[order(data$pat_id, -data$u_giltigVariabel, -data$u_giltigIntervall, data$u_datbedom),]
    data <- 
      subset(
        data,
        !duplicated(data.frame(pat_id))
      )
    data$u_uppfoljningrapporterad <- rep(1, nrow(data))
    replaceColnames <- substr(colnames(data),1, 2) == "u_"
    colnames(data)[replaceColnames] <- 
      paste0(
        prefix, 
        substr(
          colnames(data)[replaceColnames],
          3,
          100
        )
      )
    data
  }