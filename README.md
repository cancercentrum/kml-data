
# Nationella kvalitetsregistret för Kronisk Myeloisk Leukemi (KML)

Skript för clean data, beräknade variabler, etc.

## Användning

- Ladda hem .RData-fil från INCA, t.ex. från någon av vyerna:
    - *KML_Nationell_Anmälan_Uppföljning*
    - *KML_Nationell_Anmälan_Uppföljning_EjID*
    - *KML_Regional_Anmälan_Uppföljning*
- Ladda hem .RData-fil från behandlingstabellen, t.ex. från:
    - *KML_Nationell_Behandling*
- Kör **main.R** för clean data.
- Se separat repo **kml-rccShiny-ind** för definition av bl.a.
  kvalitetsindikatorer.

## Resultat

En `tbl_df` med beräknade variabler och uppföljningar, t.ex.:

    d0_ ...
    u0_ ...
    u1y_ ...
    u2y_ ...
    u5y_ ...
    u10y_ ...
